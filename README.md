# Back aplicación de tareas 

Backend de aplicación tareas(https://gitlab.com/fralfarom/aplicacion-tareas-reactjs.git)

## Instrucciones 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._



### Pre-requisitos 📋

Nada.

### Instalación 🔧

_Serie de ejemplos pasos para instalar el proyecto_


```
1.git clone https://gitlab.com/fralfarom/aplicacion-tareas-backend.git
2.cd /aplicacion-tareas-backend
3.npm install -g json-server
4.json-server db.json
```
## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [JSON-Server](https://github.com/typicode/json-server) - Libreria utilizada en este proyecto

## Autore ✒️

* **Francisco Alfaro** - *Desarrollador* - [fralfarom](https://gitlab.com/fralfarom)